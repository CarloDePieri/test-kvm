image:
	docker build docker-v2-env/. --tag debian-v2-env:latest

test:
	gitlab-runner exec docker --docker-privileged --docker-pull-policy if-not-present --docker-image debian-v2-env build-job
